# -*- coding: utf-8 -*-
"""
Created on Thu Jul 11 11:17:55 2019

@author: kruis015
rewrite two or more maf files that are concatenated and sorted
partially rewritten from the LAST maf-join utility to allow local realignment
"""
import sys
sys.path.append('/lustre/backup/WUR/ABGC/kruis015/scripts')
from functions import *
import subprocess
import argparse

parser = argparse.ArgumentParser(description='rewrite two maf files that are concatenated and sorted (based on maf-join)')
parser.add_argument("--infile", help="maf file", type=str)
parser.add_argument("--out", help="output file", type=str)
parser.add_argument("--ref", help="name of reference species")
args = parser.parse_args()


class cd:
    """Context manager for changing the current working directory"""
    def __init__(self, newPath):
        self.newPath = os.path.expanduser(newPath)

    def __enter__(self):
        self.savedPath = os.getcwd()
        os.chdir(self.newPath)

    def __exit__(self, etype, value, traceback):
        os.chdir(self.savedPath)
        
def countNonGaps(s): return len(s) - s.count('-')

def nthNonGap(s, n):
    '''Get the start position of the n-th non-gap.'''
    for i, x in enumerate(s):
        if x != '-':
            if n == 0: return i
            n -= 1
    raise ValueError('non-gap not found')

def nthLastNonGap(s, n):
    '''Get the end position of the n-th last non-gap.'''
    return len(s) - nthNonGap(s[::-1], n)

def mafSlice(block, alnBeg, alnEnd):
    '''Return a slice of a MAF block, using coordinates in the alignment.'''
    newblock = AlignmentBlock('newblock')
    refstart = block.reference.startpos + countNonGaps(block.reference.seq[:alnBeg])
    refend = block.reference.endpos - countNonGaps(block.reference.seq[alnEnd:])
    refseq = block.reference.seq[alnBeg:alnEnd]
    newblock.reference = AlignmentBlock.ReferenceLine(block.reference.species, block.reference.contig, refstart, refend - refstart,
                                                      block.reference.strand, block.reference.contiglength, refseq)
    for line in block.lines:
        linestart = line.startpos + countNonGaps(line.seq[:alnBeg])
        lineend = line.endpos - countNonGaps(line.seq[alnEnd:])
        lineseq = line.seq[alnBeg:alnEnd]
        newblock.lines.append(AlignmentBlock.AlignmentLine(line.species, line.contig, linestart, lineend - linestart,
                                                           line.strand, line.contiglength, lineseq))
    return newblock

def mafSliceTopSeq(block, newstartpos, newendpos):
    '''Return a slice of a MAF block, using coordinates in the top sequence.'''
    lettersFromBeg = newstartpos - block.reference.startpos
    lettersFromEnd = block.reference.endpos - newendpos
    alnBeg = nthNonGap(block.reference.seq, lettersFromBeg)
    alnEnd = nthLastNonGap(block.reference.seq, lettersFromEnd)
    return mafSlice(block, alnBeg, alnEnd)

def mafJoin(blocklist):
    '''join two or more maf blocks into one big maf block
    The blocks have to share their reference line coordinates'''
    assert len(blocklist) > 1
    firstblock = blocklist[0]
    fstart = firstblock.reference.startpos
    fend = firstblock.reference.endpos
    fcontig = firstblock.reference.contig
    fseq = firstblock.reference.seq.replace('-','')
    for block in blocklist[1:]:
        assert block.reference.startpos == fstart
        assert block.reference.endpos == fend
        assert block.reference.contig == fcontig
        assert block.reference.seq.replace('-','') == fseq
        for line in block.lines:
            firstblock.lines.append(AlignmentBlock.AlignmentLine(line.species, line.contig, line.startpos, line.length,
                                                           line.strand, line.contiglength, line.seq))
    
    #get sequences for realignment if block length > 1 nucleotide
    realigndict = {}
    realigndict['ref'] = firstblock.reference.seq.replace('-','')
    for i, line in enumerate(firstblock.lines):
        realigndict['line'+str(i)] = line.seq.replace('-','')
    
    len1 = [len(s) == 1 for s in realigndict.values()]
    
    if False in len1:
        #now realign and store in block
        write_fasta(realigndict, 'temp.fa')
        aln = '/lustre/backup/WUR/ABGC/kruis015/heterandria/tools/mafft-linux64/mafft.bat --auto temp.fa > temp_aligned.fa' 
        subprocess.call(aln, shell=True)
        aligned_seqs = read_fasta('temp_aligned.fa')
        firstblock.reference.seq = aligned_seqs['ref']
        for i, line in enumerate(firstblock.lines):
            line.seq = aligned_seqs['line'+str(i)]
    return firstblock

def handle_overlaps(overlapping):
    '''converts a set of overlapping blocks into a set of non-overlapping blocks'''
    all_unique_boundaries = []
    for block in overlapping:
        if block.reference.startpos not in all_unique_boundaries:
            all_unique_boundaries.append(block.reference.startpos)
        if block.reference.endpos not in all_unique_boundaries:
            all_unique_boundaries.append(block.reference.endpos)
    all_unique_boundaries = sorted(all_unique_boundaries)
    newblocks = []
    for i, b in enumerate(all_unique_boundaries[:-1]):
        slices = []
        for block in overlapping:
            if block.reference.startpos <= b and block.reference.endpos >= all_unique_boundaries[i+1]:
                slices.append(mafSliceTopSeq(block, b, all_unique_boundaries[i+1]))
        if len(slices) == 1:
            newblocks.append(slices[0])
        elif len(slices) > 1:
            newblocks.append(mafJoin(slices))
        else:
            print('something went wrong')
    return newblocks

def yield_blocks(alignments):
    #function that yields blocks after the respective portion of the genome is handled by the overlap handling function
    #works with a sorted maf file and a cache system: blocks are on first instance added to the cache. New blocks are
    #compared to blocks in the cache. If they overlap, the block pairs are replaced with sets of non-overlapping blocks
    #blocks in cache that have lower coordinates that the current block are yielded and can be written to the new maf file
    cache = []
    for block in alignments:
        if 'n' in block.reference.seq:
            pass
        else:
            fl = block.reference
            removelist = []
            newblocks = []
            for block3 in cache: #first check if blocks can be yielded
                if (block3.reference.endpos < fl.startpos and block3.reference.contig == fl.contig) or block3.reference.contig != fl.contig:
                    currentblock = cache.pop(cache.index(block3))
                    yield currentblock
            overlapping = [block] #then collect overlapping blocks
            for i, block2 in enumerate(cache):
                fl2 = block2.reference
                if fl.startpos < fl2.endpos and fl.contig == fl2.contig:
                    overlapping.append(block2)
                    removelist.append(block2)
            if len(overlapping) > 1: #if there are overlapping blocks, rewrite;
		try:
                    newblocks = handle_overlaps(overlapping)
		except (AssertionError, ValueError):
		    print('skipping one set of blocks')
            if len(removelist) == 0:
                cache.append(block)
            elif len(removelist) > 0:
                cache2 = [block for block in cache if block not in removelist]
                cache = cache2
                cache += newblocks
    
    #in the end, clean cache
    for block in cache:
        yield block    

newdir = args.infile.split('.')[0]
cmd0 = 'mkdir %s' % newdir
subprocess.call(cmd0, shell=True)

with cd('./'+newdir):
    cmd1 = 'ln -s ../%s' % args.infile
    subprocess.call(cmd1, shell=True)
    alignments = parse_maf(args.infile, args.ref)
    write_maf(yield_blocks(alignments), args.out)

#cleanup:
cmd2 = 'rm %s' % args.infile
subprocess.call(cmd2, shell=True)
cmd3 = 'ln -s %s/%s' % (newdir, args.out)
subprocess.call(cmd3, shell=True)